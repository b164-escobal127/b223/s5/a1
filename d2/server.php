<?php

session_start();

//echo $_POST['description'];
//echo $_SESSION['greet'];

class TaskList{

	// Add task
	public function add($description){
		$newTask = (object)[
			'description' => $description,
			'isFinished' => false
		];

		// If there is no added task yet.
		if($_SESSION['tasks'] === null){
			$_SESSION['tasks'] = array();
		}

		// Then $newTask will be added in the $_SESSION['tasks'] variable.
		array_push($_SESSION['tasks'], $newTask);

	}

	//update a task
	// the update task will be needing three parameters:
		// $id for searching specific task.
		// $description and $isFinished

	public function update($id, $description, $isFinished){
		// task[$id] same wuth this variable
		$_SESSION['tasks'][$id]->description = $description;
		$_SESSION['tasks'][$id]->isFinished = ($isFinished !== null) ? true : false;
	}

	// Delete a task
	public function remove($id){
		// array_splice(array, startDel, length, newArrElement)
		array_splice($_SESSION['tasks'], $id, 1);
	}

	// Remove all the tasks
	public function clear(){
		//removes all of the data associated with current session.
		session_destroy();
	}
}

//taskList is instantiated from the TaskList() class to have access with its method.
$taskList = new TaskList();

//This will handle the action sent by the user.
if($_POST['action'] === 'add'){
	$taskList->add($_POST['description']);
}
else if ($_POST['action'] === 'update'){
	$taskList->update($_POST['id'], $_POST['description'], $_POST['isFinished']);
}
else if($_POST['action'] === 'remove'){
	$taskList->remove($_POST['id']);
}
else if($_POST['action'] === 'clear'){
	$taskList->clear();
}
// it will redirect us to the index file upon sending the request.
header('Location: ./index.php');



